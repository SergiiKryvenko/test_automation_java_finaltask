Feature: Smoke
  As a user
  I want to test all main site functional
  So that I can be sure that site works correctly

  Scenario Outline: Check base URL
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks search field visibility
    Then User checks URL '<urlAddress>'

    Examples: 
      | homePage              | urlAddress            |
      | https://www.ebay.com/ | https://www.ebay.com/ |

  Scenario Outline: Check Register page
    Given User opens '<homePage>' page
    And User checks 'Register' link visibility on Home page
    When User clicks 'Register' button on Home page
    And User checks 'title' message '<titleMessage>' on Register page
    And User checks 'Sign in' link visibility on Register page
    And User checks 'First Name' field visibility on Register page
    And User checks 'Last Name' field visibility on Register page
    And User checks 'Email' field visibility on Register page
    And User checks 'Password' field visibility on Register page
    Then User checks 'Register' page URL '<urlAddress>'

    Examples: 
      | homePage              | titleMessage 			| urlAddress               |
      | https://www.ebay.com/ | Create an account | https://signup.ebay.com/ |
      
  Scenario Outline: Check SignIn page
    Given User opens '<homePage>' page
    And User checks 'Sign in' link visibility on Home page
    When User clicks 'Sign in' button on Home page
    And User checks 'title' message '<titleMessage>' on SignIn page
    And User checks email field visibility on SignIn page
    And User checks 'Continue' button visibility on SignIn page
    And User checks 'Continue with Facebook' button visibility on SignIn page
    And User checks 'Continue with Google' button visibility on SignIn page
    And User checks 'Continue with Apple' button visibility on SignIn page
    Then User checks 'Sign in' page URL '<urlAddress>'

    Examples: 
      | homePage              | titleMessage | urlAddress               |
      | https://www.ebay.com/ | Hello        | https://signin.ebay.com/ |

   Scenario Outline: Check Search page
    Given User opens '<homePage>' page
    And User checks 'Search' field visibility on Home page
    And User checks 'Search' button visibility on Home page
    When User makes search by keyword '<keyword>' on Home page
    And User clicks 'Search' button on Home page
    Then User checks 'Search' page URL '<urlAddress>'

    Examples: 
      | homePage              | keyword  | urlAddress    			     	  |
      | https://www.ebay.com/ | garmin   | https://www.ebay.com/sch/	|
      
   Scenario Outline: Check Shoping cart
    Given User opens '<homePage>' page
    And User checks 'Shoping cart' icon visibility on Home page
    And User checks 'Search' field visibility on Home page
    And User checks 'Search' button visibility on Home page
    When User makes search by keyword '<keyword>' on Home page
    And User clicks 'Search' button on Home page
    And User clicks to the first 'Items' on Search results page
    And User changes 'Quantity' of items to '<numbersOfItems>' on Product page
    And User clicks to 'Add to cart' button on Product page
    Then User checks numbers of items on 'Shoping cart' icon '<shopingCartItems>'

    Examples: 
      | homePage              | keyword  | numbersOfItems	| shopingCartItems |
      | https://www.ebay.com/ | garmin   | 5							| 5								 |

   Scenario Outline: Check Checkout page
    Given User opens '<homePage>' page
    And User checks 'Shoping cart' icon visibility on Home page
    And User checks 'Search' field visibility on Home page
    And User checks 'Search' button visibility on Home page
    When User makes search by keyword '<keyword>' on Home page
    And User clicks 'Search' button on Home page
    And User clicks to the first 'Items' on Search results page
    And User changes 'Quantity' of items to '<numbersOfItems>' on Product page
    And User clicks to 'Buy it Now' button on Product page and chooses as a Guest
    Then User checks 'Checkout' page URL '<urlAddress>'

    Examples: 
      | homePage              | keyword  | numbersOfItems	| urlAddress						|
      | https://www.ebay.com/ | garmin   | 5							| https://pay.ebay.com/	|
      
   Scenario Outline: Check chooses of category
    Given User opens '<homePage>' page
    And User checks 'Shop By Category' button visibility on Home page
    When User clicks to 'Shop By Category' button on Home page
    And User clicks to category '<category>' from drop-down list on Home page
    Then User checks 'Category' page URL '<urlAddress>'

    Examples: 
      | homePage              | category		| urlAddress												|
      | https://www.ebay.com/ | Antiques		| https://www.ebay.com/b/Antiques/	|
      
   Scenario Outline: Check chooses of category from top menu
    Given User opens '<homePage>' page
    When User clicks to category '<category>' from from top menu on Home page
    Then User checks 'Category' page URL '<urlAddress>'

    Examples: 
      | homePage              | category	| urlAddress														|
      | https://www.ebay.com/ | Sports		| https://www.ebay.com/b/Sporting-Goods	|
      
    Scenario Outline: Check 'Daily Details' link from header
    Given User opens '<homePage>' page
    And User checks 'Daily Details' link visibility on Home page
    When User clicks to 'Daily Details' link on Home page
    Then User checks 'Daily Details' page URL '<urlAddress>'

    Examples: 
      | homePage              | urlAddress												|
      | https://www.ebay.com/ | https://www.ebay.com/globaldeals	|
      
   Scenario Outline: Check 'Help & Contacts' link from header
    Given User opens '<homePage>' page
    And User checks 'Help & Contacts' link visibility on Home page
    When User clicks to 'Help & Contacts' link on Home page
    Then User checks 'Help & Contacts' page URL '<urlAddress>'

    Examples: 
      | homePage              | urlAddress											|
      | https://www.ebay.com/ | https://www.ebay.com/help/home	|
      
   Scenario Outline: Check error message when login with wrong email
    Given User opens '<homePage>' page
    And User checks 'Sign in' link visibility on Home page
    And User clicks 'Sign in' button on Home page
    And User checks email field visibility on SignIn page
    When User enters to email field wrong email '<email>' on SignIn page
    And User clicks 'Continue' button on SignIn page
    And User checks error message visibility on SignIn page
    Then User checks error message '<errorMessage>' on SignIn page

    Examples: 
      | homePage              | email								| errorMessage	|
      | https://www.ebay.com/ | some_email@mail.com	| Oops, that		|
