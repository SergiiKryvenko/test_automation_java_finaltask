package manager;

import org.openqa.selenium.WebDriver;

import pages.CheckOutPurchasePage;
import pages.DailyDetailsPage;
import pages.HelpAndContactsPage;
import pages.HomePage;
import pages.ProductPage;
import pages.RegisterPage;
import pages.SearchResultsPage;
import pages.SignInPage;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }
    
    public SignInPage getSignInPage() {
        return new SignInPage(driver);
    }
    
    public RegisterPage getRegisterPage() {
        return new RegisterPage(driver);
    }
    
    public SearchResultsPage getSearchResultsPage() {
        return new SearchResultsPage(driver);
    }
    
    public ProductPage getProductPage() {
        return new ProductPage(driver);
    }
    
    public CheckOutPurchasePage getCheckOutPurchasePage() {
        return new CheckOutPurchasePage(driver);
    }
    
    public DailyDetailsPage getDailyDetailsPage() {
        return new DailyDetailsPage(driver);
    }
    
    public HelpAndContactsPage getHelpAndContactsPage() {
        return new HelpAndContactsPage(driver);
    }
}
