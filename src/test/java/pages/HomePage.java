package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

public class HomePage extends BasePage {

    @FindBy(css = "header[role='banner']")
    protected WebElement header;

    @FindBy(xpath = "//li[@id='gh-minicart-hover']//a[contains(@href,'cart.payments.ebay.com/sc/view')]")
    protected WebElement cartIcon;

    @FindBy(xpath = "//div[@role='navigation']//span[@class='gh-ug-guest']/a[contains(@href,'signin.ebay.com')]")
    protected WebElement signInButton;

    @FindBy(xpath = "//div[@role='navigation']//span[@class='gh-ug-guest']//a[contains(@href,'reg.ebay.com')]")
    protected WebElement registerButton;

    @FindBy(css = "#gh-top a[href*='globaldeals']")
    protected WebElement dalyDetailsLink;

    @FindBy(css = "#gh-top a[href*='/ocs/home']")
    protected WebElement helpAndContactsLink;

    @FindBy(xpath = "//a[@href='https://www.ebay.com/']")
    protected WebElement logo;

    @FindBy(css = "input[type='text']")
    protected WebElement searchField;

    @FindBy(id = "gh-btn")
    protected WebElement searchButton;

    @FindBy(css = "#gh-cart-n")
    protected WebElement cartIconProductsCount;

    @FindBy(id = "gh-shop-a")
    protected WebElement shopByCategoryDropListButton;

    @FindBy(css = "table[role='presentation'] li>a.scnd")
    protected List<WebElement> categoriesInDropList;

    @FindBy(css = "#mainContent .hl-cat-nav__container>li[data-currenttabindex]>a")
    private List<WebElement> categoriesTopMenuTabs;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @Step(value = "open URL address {0}")
    public void openHomePage(String url) {
        driver.get(url);
    }

    @Step(value = "check is Header is visible")
    public boolean isHeaderVisible() {
        logger.info("check is Header is visible ");
        return header.isDisplayed();
    }

    @Step(value = "check is Cart Icon is visible")
    public boolean isCartIconVisible() {
        return cartIcon.isDisplayed();
    }

    @Step(value = "check is SignIn Button is visible")
    public boolean isSignInButtonVisible() {
        return signInButton.isDisplayed();
    }

    @Step(value = "click to SignIn Button")
    public SignInPage clickSignInButton() {
        signInButton.click();
        return new SignInPage(driver);
    }

    @Step(value = "check is Register Button is visible")
    public boolean isRegisterButtonVisible() {
        return registerButton.isDisplayed();
    }

    @Step(value = "click to Register Button")
    public RegisterPage clickRegisterButton() {
        registerButton.click();
        return new RegisterPage(driver);
    }

    @Step(value = "check is Search field is visible")
    public boolean isSearchFieldVisible() {
        logger.info("check is Search field is visible");
        return searchField.isDisplayed();
    }

    @Step(value = "check is Search button is visible")
    public boolean isSearchButtonVisible() {
        logger.info("check is Search button is visible");
        return searchButton.isDisplayed();
    }

    @Step(value = "click to ShopingCart icon")
    public ShopingCartPage clickCartButton() {
        logger.info("click to ShopingCart icon");
        cartIcon.click();
        return new ShopingCartPage(driver);
    }

    @Step(value = "enter text to SearchField: {0}")
    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        logger.info("input in Serach field text " + searchText);
        searchField.sendKeys(searchText);
    }

    @Step(value = "click to Search Button")
    public SearchResultsPage clickSearchButton() {
        logger.info("click to Search Button");
        searchButton.click();
        return new SearchResultsPage(driver);
    }

    @Step(value = "enter text to SearchField: {0} and click Search")
    public SearchResultsPage enterTextToSearchFieldAndClickSearch(final String searchText) {
        logger.info("start Search text " + searchText);
        enterTextToSearchField(searchText);
        clickSearchButton();
        return new SearchResultsPage(driver);
    }

    @Step(value = "get amount of products in the Cart")
    public String getAmountOfProductsInCart() {
        wait.visibilityOfWebElement(cartIconProductsCount);
        String productsInCart = cartIconProductsCount.getText();
        logger.info("quantity products in the Cart = " + productsInCart);
        return productsInCart;
    }

    @Step(value = "get Home page URL address")
    public String getHomeURL() {
        String currentURL = driver.getCurrentUrl();
        logger.info("current URL = " + currentURL);
        return currentURL;
    }

    @Step(value = "check is 'Shop By Category' drop-down button is visible")
    public boolean isShopByCategoryDropListButtonVisible() {
        logger.info("check is 'Shop By Category' drop-down button is visible");
        return shopByCategoryDropListButton.isDisplayed();
    }

    @Step(value = "click to 'Shop By Category' Button")
    public void clickShopByCategoryDropListButton() {
        logger.info("click to 'Shop By Category' Button");
        shopByCategoryDropListButton.click();
    }

    @Step(value = "click to the category {0} from drop-down list")
    public boolean clickToCategoryFromDropDownList(String category) {
        for (WebElement categoryWebElement : categoriesInDropList) {
            if (categoryWebElement.getText().equalsIgnoreCase(category)) {
                logger.info("click to certain 'Category': " + category);
                categoryWebElement.click();
                return true;
            }
        }
        return false;
    }

    @Step(value = "click to the category tab {0} from top menu")
    public boolean clickToCategoryFromTopMenu(String categoryTab) {
        for (WebElement categoryWebElement : categoriesTopMenuTabs) {
            if (categoryWebElement.getText().equalsIgnoreCase(categoryTab)) {
                categoryWebElement.click();
                return true;
            }
        }
        return false;
    }

    @Step(value = "check is 'Daly Details' link is visible")
    public boolean isDalyDetailsLinkVisible() {
        logger.info("check is 'Daly Details' link is visible");
        return dalyDetailsLink.isDisplayed();
    }

    @Step(value = "click to 'Daly Details' link")
    public DailyDetailsPage clickDalyDetailsLink() {
        logger.info("click to 'Daly Details' link");
        dalyDetailsLink.click();
        return new DailyDetailsPage(driver);
    }

    @Step(value = "check is 'Help & Contacts' link is visible")
    public boolean isHelpAndContactsVisible() {
        logger.info("check is 'Help & Contacts' link is visible");
        return helpAndContactsLink.isDisplayed();
    }

    @Step(value = "click to 'Help & Contacts' link")
    public HelpAndContactsPage clickHelpAndContactsLink() {
        logger.info("click to 'Help & Contacts' link");
        helpAndContactsLink.click();
        return new HelpAndContactsPage(driver);
    }

}