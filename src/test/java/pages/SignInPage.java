package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

/**
 * Class describing SignIn page (https://signin.ebay.com/...)
 */
public class SignInPage extends BasePage {

    @FindBy(xpath = "//a[@href='https://www.ebay.com/']")
    private WebElement logo;

    @FindBy(id = "greeting-msg")
    private WebElement greetingMessage;

    @FindBy(id = "create-account-link")
    private WebElement createAccountLink;

    @FindBy(id = "userid")
    private WebElement userNameField;

    @FindBy(id = "signin-continue-btn")
    private WebElement continueButton;

    @FindBy(id = "signin_fb_btn")
    private WebElement continueWithFacebookButton;

    @FindBy(id = "signin_ggl_btn")
    private WebElement continueWithGoogleButton;

    @FindBy(id = "signin_appl_btn")
    private WebElement continueWithAppleButton;

    @FindBy(id = "kmsi-checkbox")
    private WebElement staySignedInChckbox;

    @FindBy(id = "kmsi-learn-more-link")
    private WebElement learnMoreLink;

    private final String errorMessageCssSelector = "#signin-form p#signin-error-msg";

    @FindBy(css = errorMessageCssSelector)
    private WebElement errorMessage;

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    @Step(value = "get title text")
    public String getGreetingMessage() {
        logger.info("get title text from SignIn page");
        return greetingMessage.getText();
    }

    @Step(value = "click to Logo")
    public HomePage clickOnLogo() {
        logger.info("click to Logo on SignIn page");
        logo.click();
        return new HomePage(driver);
    }

    @Step(value = "click to CreateAccount link")
    public RegisterPage clickCreateAccountLink() {
        logger.info("click to CreateAccount link");
        createAccountLink.click();
        return new RegisterPage(driver);
    }

    @Step(value = "check is 'User Name' field is visible")
    public boolean isDisplayedUserNameField() {
        logger.info("check is 'User Name' field is visible");
        return userNameField.isDisplayed();
    }

    @Step(value = "enter text to UserName: {0}")
    public void enterTextToUserNameField(final String userName) {
        logger.info("enter text to UserName: " + userName);
        userNameField.sendKeys(userName);
    }

    @Step(value = "check is Continue Button is visible")
    public boolean isDisplayedContinueButton() {
        logger.info("check is Continue Button is visible");
        return continueButton.isDisplayed();
    }

    @Step(value = "click to Continue buton")
    public BasePage clickContinueButon() {
        logger.info("click to Continue buton");
        if (continueButton.isEnabled()) {
            continueButton.click();
            return new HomePage(driver);
        } else {
            return this;
        }
    }

    @Step(value = "check is error message is visible")
    public boolean isDisplayedErrorMessage() {
        wait.presenceOfElementLocated(By.cssSelector(errorMessageCssSelector));
        logger.info("check is error message is visible");
        return errorMessage.isDisplayed();
    }

    @Step(value = "get error message")
    public String getErrorMessage() {
        wait.visibilityOfWebElement(errorMessage);
        String error = errorMessage.getText();
        logger.info("get error message: " + error);
        return error;
    }

    @Step(value = "check is 'Continue With Facebook' button is visible")
    public boolean isDisplayedContinueWithFacebookButton() {
        logger.info("check is 'Continue With Facebook' button is visible");
        return continueWithFacebookButton.isDisplayed();
    }

    @Step(value = "check is 'Continue With Google' button is visible")
    public boolean isDisplayedContinueWithGoogleButton() {
        logger.info("check is 'Continue With Google' button is visible");
        return continueWithGoogleButton.isDisplayed();
    }

    @Step(value = "check is 'Continue With Apple' button is visible")
    public boolean isDisplayedContinueWithAppleButton() {
        logger.info("check is 'Continue With Apple' button is visible");
        return continueWithAppleButton.isDisplayed();
    }

    @Step(value = "check is 'Stay Signed In' link is visible")
    public boolean isDisplayedStaySignedInChckbox() {
        logger.info("check is 'Stay Signed In' link is visible");
        return staySignedInChckbox.isDisplayed();
    }

    @Step(value = "check is 'Learn More' link is visible")
    public boolean isDisplayedLearnMoreLink() {
        logger.info("check is 'Learn More' link is visible");
        return learnMoreLink.isDisplayed();
    }
}
