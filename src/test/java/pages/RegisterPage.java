package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

public class RegisterPage extends BasePage {

    @FindBy(xpath = "//a[@href='https://www.ebay.com/']")
    private WebElement logo;

    @FindBy(css = "div[data-page='emailReg']>span>a[href='#']")
    private WebElement signInLink;

    @FindBy(css = "h1[class*='txt-center']")
    private WebElement titleMessage;
    
    private final String personalAccountRadiobuttonCssSelector = "div.switch-account-type-reg input[value='personalaccount']";
    
    @FindBy(css = personalAccountRadiobuttonCssSelector)
    private WebElement personalAccountRadiobutton;
    
    private final String businessAccountRadiobuttonCssSelector = "div.switch-account-type-reg input[value='businessaccount']";

    @FindBy(css = businessAccountRadiobuttonCssSelector)
    private WebElement businessAccountRadiobutton;
    
    private final String createBusinessAccountLinkCssSelector = "div[data-page='emailReg'] a[href*='acntType=business']";
    
    @FindBy(css = createBusinessAccountLinkCssSelector)
    private WebElement createBusinessAccountLink;

    @FindBy(id = "firstname")
    private WebElement firstNameField;

    @FindBy(id = "lastname")
    private WebElement lastNameField;

    @FindBy(id = "Email")
    private WebElement emailField;

    @FindBy(id = "password")
    private WebElement passwordField;
    
    private final String showPasswordCheckboxCssSelector = "div#showpasswordcontainer input#showpassword";

    @FindBy(css = showPasswordCheckboxCssSelector)
    private WebElement showPasswordCheckbox;

    @FindBy(id = "EMAIL_REG_FORM_SUBMIT")
    private WebElement createAccountButton;

    @FindBy(id = "facebook")
    private WebElement continueWithFacebookButton;

    @FindBy(id = "google")
    private WebElement continueWithGoogleButton;

    @FindBy(id = "apple")
    private WebElement continueWithAppleButton;

    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    @Step(value = "click to Logo")
    public HomePage clickOnLogo() {
        logger.info("click to Logo");
        logo.click();
        return new HomePage(driver);
    }
    
    @Step(value = "check is 'SignIn' link is visible")
    public boolean isDisplayedSignInLink() {
        logger.info("check is 'SignIn' link is visible");
        wait.visibilityOfWebElement(signInLink);
        return signInLink.isDisplayed();
    } 

    @Step(value = "click to SignIn Link")
    public SignInPage clickSignInLink() {
        logger.info("click to SignIn Link");
        signInLink.click();
        return new SignInPage(driver);
    }
    
    @Step(value = "get title message")
    public String getTitleMessage() {
        logger.info("get title message");
        wait.visibilityOfWebElement(titleMessage);
        return titleMessage.getText();
    }
    
    @Step(value = "check is 'Business account' radiobutton or 'Create Business Account' link is visible")
    public boolean isDisplayedBusinessAccountLRadiobutton() {
        logger.info("check is 'Business account' radiobutton or 'Create Business Account' link is visible");
        if (wait.presenceOfElementLocated(By.cssSelector(personalAccountRadiobuttonCssSelector))) {
            wait.visibilityOfWebElement(businessAccountRadiobutton);
            return businessAccountRadiobutton.isDisplayed();
        } else {
            wait.visibilityOfWebElement(createBusinessAccountLink);
            return createBusinessAccountLink.isDisplayed();
        }
    }

    @Step(value = "check is 'First Name' field is visible")
    public boolean isDisplayedFirstNameField() {
        logger.info("check is 'First Name' field is visible");
        return firstNameField.isDisplayed();
    }

    @Step(value = "enter text to 'First Name' field: {0}")
    public void enterTextToFirstNameField(final String firstName) {
        logger.info("enter text to 'First Name' field: " + firstName);
        firstNameField.sendKeys(firstName);
    }

    @Step(value = "check is 'Last Name' field is visible")
    public boolean isDisplayedLastNameField() {
        logger.info("check is 'Last Name' field is visible");
        return lastNameField.isDisplayed();
    }

    @Step(value = "enter text to 'Last Name' field: {0}")
    public void enterTextToLastNameField(final String lastName) {
        logger.info("enter text to 'Last Name' field: " + lastName);
        lastNameField.sendKeys(lastName);
    }

    @Step(value = "check is 'Email' field is visible")
    public boolean isDisplayedEmailField() {
        logger.info("check is 'Email' field is visible");
        return emailField.isDisplayed();
    }

    @Step(value = "enter text to 'Email' field: {0}")
    public void enterTextToEmailField(final String email) {
        logger.info("enter text to 'Email' field: " + email);
        emailField.sendKeys(email);
    }

    @Step(value = "check is 'Password' field is visible")
    public boolean isDisplayedPasswordField() {
        logger.info("check is 'Password' field is visible");
        return passwordField.isDisplayed();
    }

    @Step(value = "enter text to 'Password' field: {0}")
    public void enterTextToPasswordField(final String password) {
        logger.info("enter text to 'Password' field: " + password);
        passwordField.sendKeys(password);
    }
    
    @Step(value = "check is 'Show' password checkbox is visible")
    public boolean isDisplayedShowPasswordCheckbox() {
        logger.info("check is 'Show' password checkbox is visible");
        wait.presenceOfElementLocated(By.cssSelector(showPasswordCheckboxCssSelector));
        return showPasswordCheckbox.isDisplayed();
    }
    
    @Step(value = "click to 'Show' password checkbox")
    public void clickShowPasswordCheckbox() {
        logger.info("click to 'Show' password checkbox");
        wait.presenceOfElementLocated(By.cssSelector(showPasswordCheckboxCssSelector));
        showPasswordCheckbox.click();
    }
    
    @Step(value = "check is 'Continue With Facebook' button is visible")
    public boolean isDisplayedContinueWithFacebookButton() {
        logger.info("check is 'Continue With Facebook' button is visible");
        return continueWithFacebookButton.isDisplayed();
    }
    
    @Step(value = "check is 'Continue With Google' button is visible")
    public boolean isDisplayedContinueWithGoogleButton() {
        logger.info("check is 'Continue With Google' button is visible");
        return continueWithGoogleButton.isDisplayed();
    }
    
    @Step(value = "check is 'Continue With Apple' button is visible")
    public boolean isDisplayedContinueWithAppleButton() {
        logger.info("check is 'Continue With Apple' button is visible");
        return continueWithAppleButton.isDisplayed();
    }
}
