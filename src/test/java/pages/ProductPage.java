package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.qameta.allure.Step;

public class ProductPage extends HomePage {

    @FindBy(css = "div[class*='box-marpad'] a#binBtn_btn")
    private WebElement buyItNowButton;

    @FindBy(xpath = "//div[contains(@class,'box-marpad')]//a[@id='isCartBtn_btn']")
    private WebElement addToCartButton;

    @FindBy(xpath = "//div[contains(@class,'quantity')]//input")
    private WebElement quantityField;

    @FindBy(xpath = "//div[@id='watchWrapperId']//span[text()='Add to Watchlist']")
    private WebElement addToWatchlistButton;

    @FindBy(css = "div.nonActPanel div#vi-itm-cond")
    private WebElement conditionField;

    @FindBy(css = "div#streamline-bin-layer button#sbin-signin-btn")
    private WebElement gusetPopUpSignIn;

    @FindBy(css = "div#streamline-bin-layer button#sbin-gxo-btn")
    private WebElement gusetPopUpCheckOut;

    @FindBy(id = "prcIsum")
    private WebElement price;

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    @Step(value = "click to the 'Add To Cart' button")
    public ShopingCartPage clickToAddToCartButton() {
        logger.info("Product page: click 'Add To Cart' button");
        addToCartButton.click();
        return new ShopingCartPage(driver);
    }

    @Step(value = "click to the 'Buy It Now' button")
    public CheckOutPurchasePage clickToBuyItNowButton() {
        logger.info("Product page: click 'Buy It Now' button");
        buyItNowButton.click();
        gusetPopUpCheckOut.click();
        return new CheckOutPurchasePage(driver);
    }

    @Step(value = "get Condition Of Product")
    public String getConditionOfProduct() {
        logger.info("get Condition Of Product");
        return conditionField.getText();
    }

    @Step(value = "Product page: entered quantity of products = {0}")
    public ProductPage enterQuantityOfProducts(String quantity) {
        quantityField.clear();
        logger.info("Product page: entered quantity of products = " + quantity);
        quantityField.sendKeys(quantity);
        return this;
    }

}
