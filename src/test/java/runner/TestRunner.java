package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = "json:target/cucumber-report.json", 
                 features = "src/test/resources", 
                 glue = "stepdefinitions")
public class TestRunner extends AbstractTestNGCucumberTests {
}
