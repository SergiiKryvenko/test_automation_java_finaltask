package stepdefinitions;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import pages.CheckOutPurchasePage;
import pages.DailyDetailsPage;
import pages.HelpAndContactsPage;
import pages.HomePage;
import pages.ProductPage;
import pages.RegisterPage;
import pages.SearchResultsPage;
import pages.SignInPage;

public class DefinitionSteps {

    WebDriver driver;
    HomePage homePage;
    SignInPage signInPage;
    RegisterPage registerPage;
    SearchResultsPage searchResultsPage;
    ProductPage productPage;
    CheckOutPurchasePage checkOutPurchasePage;
    DailyDetailsPage dailyDetailsPage;
    HelpAndContactsPage helpAndContactsPage;
    PageFactoryManager pageFactoryManager;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @Given("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }

    @And("User checks header visibility")
    public void checkHeaderVisibility() {
        assertTrue(homePage.isHeaderVisible());
    }

    @And("User checks search field visibility")
    public void checkSearchVisibility() {
        assertTrue(homePage.isSearchFieldVisible());
    }

    @And("User checks URL {string}")
    public void checkURL(final String urlAddress) {
        assertTrue(homePage.getCurrentUrl().equalsIgnoreCase(urlAddress));
    }

    @And("User checks 'Sign in' link visibility on Home page")
    public void checkSignInLinkVisibility() {
        assertTrue(homePage.isSignInButtonVisible());
    }

    @When("User clicks 'Sign in' button on Home page")
    public void clickSignInButton() {
        signInPage = homePage.clickSignInButton();
    }

    @And("User checks 'title' message {string} on SignIn page")
    public void checkSignInTitleMessage(final String titleMessage) {
        assertTrue(signInPage.getGreetingMessage().equalsIgnoreCase(titleMessage));
    }

    @And("User checks email field visibility on SignIn page")
    public void checkEmailFieldVisibility() {
        assertTrue(signInPage.isDisplayedUserNameField());
    }

    @And("User checks 'Continue' button visibility on SignIn page")
    public void checkContinueButonVisibility() {
        assertTrue(signInPage.isDisplayedContinueButton());
    }

    @And("User checks 'Continue with Facebook' button visibility on SignIn page")
    public void checkContinueWithFacebookButtonVisibility() {
        assertTrue(signInPage.isDisplayedContinueWithFacebookButton());
    }

    @And("User checks 'Continue with Google' button visibility on SignIn page")
    public void checkContinueWithGoogleButonVisibility() {
        assertTrue(signInPage.isDisplayedContinueWithGoogleButton());
    }

    @And("User checks 'Continue with Apple' button visibility on SignIn page")
    public void checkContinueWithAppleButonVisibility() {
        assertTrue(signInPage.isDisplayedContinueWithAppleButton());
    }

    @And("User checks 'Sign in' page URL {string}")
    public void checkSignInPage(final String urlAddress) {
        assertTrue(signInPage.getCurrentUrl().contains(urlAddress));
    }

    @And("User checks 'Register' link visibility on Home page")
    public void checkRegisterLinkVisibility() {
        assertTrue(homePage.isRegisterButtonVisible());
    }

    @When("User clicks 'Register' button on Home page")
    public void clickRegisterButton() {
        registerPage = homePage.clickRegisterButton();
    }

    @And("User checks 'title' message {string} on Register page")
    public void checkRegisterTitleMessage(final String titleMessage) {
        assertTrue(registerPage.getTitleMessage().equalsIgnoreCase(titleMessage));
    }

    @And("User checks 'Sign in' link visibility on Register page")
    public void checkSignInLinkOnRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedSignInLink());
    }

    @And("User checks 'Business account' radiobutton visibility on Register page")
    public void checkCreateBusinessAccountLinkOnRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedBusinessAccountLRadiobutton());
    }

    @And("User checks 'First Name' field visibility on Register page")
    public void checkFirstNameFieldOnRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedFirstNameField());
    }

    @And("User checks 'Last Name' field visibility on Register page")
    public void checkLastNameFieldOnRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedLastNameField());
    }

    @And("User checks 'Email' field visibility on Register page")
    public void checkEmailFieldOnRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedEmailField());
    }

    @And("User checks 'Password' field visibility on Register page")
    public void checkPasswordFieldOnRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedPasswordField());
    }

    @And("User checks 'Show' password checkbox visibility on Register page")
    public void checkShowPasswordCheckboxOnRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedShowPasswordCheckbox());
    }

    @And("User checks 'Continue with Facebook' button visibility on Register page")
    public void checkContinueWithFacebookButtonRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedContinueWithFacebookButton());
    }

    @And("User checks 'Continue with Google' button visibility on Register pag")
    public void checkContinueWithGoogleButonRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedContinueWithGoogleButton());
    }

    @And("User checks 'Continue with Apple' button visibility on Register page")
    public void checkContinueWithAppleButonRegisterPageVisibility() {
        assertTrue(registerPage.isDisplayedContinueWithAppleButton());
    }

    @And("User checks 'Register' page URL {string}")
    public void checkRegisterPage(final String urlAddress) {
        assertTrue(registerPage.getCurrentUrl().contains(urlAddress));
    }

    @And("User checks 'Search' field visibility on Home page")
    public void checkSearchFieldVisibility() {
        assertTrue(homePage.isSearchFieldVisible());
    }

    @And("User checks 'Search' button visibility on Home page")
    public void checkSearchButtonVisibility() {
        assertTrue(homePage.isSearchButtonVisible());
    }

    @And("User makes search by keyword {string} on Home page")
    public void enterKeywordToSearchField(final String keyword) {
        homePage.enterTextToSearchField(keyword);
    }

    @And("User clicks 'Search' button on Home page")
    public void clickSearchButton() {
        searchResultsPage = homePage.clickSearchButton();
    }

    @And("User checks 'Search' page URL {string}")
    public void checkSearchResultsPageURL(final String urlAddress) {
        assertTrue(searchResultsPage.getCurrentUrl().contains(urlAddress));
    }

    @And("User checks 'Shoping cart' icon visibility on Home page")
    public void checkCartIconVisibility() {
        assertTrue(homePage.isCartIconVisible());
    }

    @And("User clicks to the first 'Items' on Search results page")
    public void clickFirstItemSearchPage() {
        productPage = searchResultsPage.clickToFirstProduct();
    }

    @And("User changes 'Quantity' of items to {string} on Product page")
    public void changeNumbersItemsProductPage(final String quantity) {
        productPage.enterQuantityOfProducts(quantity);
    }

    @And("User clicks to 'Add to cart' button on Product page")
    public void clickProductPage() {
        productPage.clickToAddToCartButton();
    }

    @Then("User checks numbers of items on 'Shoping cart' icon {string}")
    public void checkQuantityInCartProductPage(final String expectedItems) {
        assertTrue(productPage.getAmountOfProductsInCart().equalsIgnoreCase(expectedItems));
    }

    @And("User clicks to 'Buy it Now' button on Product page and chooses as a Guest")
    public void clickBuyItNowButton() {
        checkOutPurchasePage = productPage.clickToBuyItNowButton();
    }

    @And("User checks 'Checkout' page URL {string}")
    public void checkCheckoutPageURL(final String urlAddress) {
        assertTrue(checkOutPurchasePage.getCurrentUrl().contains(urlAddress));
    }
    
    @And("User checks 'Shop By Category' button visibility on Home page")
    public void checkCategoryDropListButtonVisible() {
        assertTrue(homePage.isShopByCategoryDropListButtonVisible());
    }
            
    @And("User clicks to 'Shop By Category' button on Home page")
    public void clickShopByCategoryButton() {
        homePage.clickShopByCategoryDropListButton();
    }
                    
    @And("User clicks to category {string} from drop-down list on Home page")
    public void clickToCategoryFromDropDownList(final String category) {
        assertTrue(homePage.clickToCategoryFromDropDownList(category));
    }
                            
    @And("User checks 'Category' page URL {string}")
    public void checkCategoryPageURL(final String urlAddress) {
        assertTrue(homePage.getCurrentUrl().contains(urlAddress));
    }
    
    @And("User clicks to category {string} from from top menu on Home page")
    public void clickToCategoryFromTopMenu(final String category) {
        assertTrue(homePage.clickToCategoryFromTopMenu(category));
    }
    
    @And("User checks 'Daily Details' link visibility on Home page")
    public void checkDalyDetailsLinkVisible() {
        assertTrue(homePage.isDalyDetailsLinkVisible());
    }
            
    @And("User clicks to 'Daily Details' link on Home page")
    public void clickDalyDetailsLink() {
        dailyDetailsPage = homePage.clickDalyDetailsLink();
    }
                    
    @And("User checks 'Daily Details' page URL {string}")
    public void checkDalyDetailsPageURL(final String urlAddress) {
        assertTrue(dailyDetailsPage.getCurrentUrl().contains(urlAddress));
    }
                            
    @And("User checks 'Help & Contacts' link visibility on Home page")
    public void checkHelpAndContactsVisible() {
        assertTrue(homePage.isHelpAndContactsVisible());
    }
                                    
    @And("User clicks to 'Help & Contacts' link on Home page")
    public void clickHelpAndContactsLink() {
        helpAndContactsPage = homePage.clickHelpAndContactsLink();
    }
                                            
    @And("User checks 'Help & Contacts' page URL {string}")
    public void checkHelpAndContactsPageURL(final String urlAddress) {
        assertTrue(helpAndContactsPage.getCurrentUrl().contains(urlAddress));
    }
    
    @And("User enters to email field wrong email {string} on SignIn page")
    public void enterWrongEmailonSignInPage(final String email) {
        signInPage.enterTextToUserNameField(email);
    }
    
    @And("User clicks 'Continue' button on SignIn page")
    public void clickContinueButonSignInPage() {
        signInPage.clickContinueButon();
    }
    
    @And("User checks error message visibility on SignIn page")
    public void checkErrorMessageVisible() {
        assertTrue(signInPage.isDisplayedErrorMessage());
    }
    
    @Then("User checks error message {string} on SignIn page")
    public void checkErrorMessageSignInPage(final String errorMessage) {
        assertTrue(signInPage.getErrorMessage().contains(errorMessage));
    }

    @After
    public void tearDown() {
        driver.close();
    }
}
